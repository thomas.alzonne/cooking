@extends('template.app')

@section('title', 'Créer - categorie')

@section('contenu')
<div class="card">
    <div class="card-header">Créer une categorie</div>
    <div class="card-content">
        <form class="form-material" method="POST" action="{{route('categories.store')}}">
            @csrf
            <div class="grix xs1">
                <div class="form-field">
                    <input type="text" id="title" name="name" class="form-control" />
                    <label for="name">Nom</label>
                </div>
                <div class="form-field">
                    <label for="description">Description</label>
                    <textarea id="description" name="description" class="form-control"></textarea>
                </div>
            </div>
            <button type="submit" class="btn press primary">Créer</button>
        </form>
    </div>
</div>
@endsection
