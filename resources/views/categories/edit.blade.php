@extends('template.admin')

@section('title', 'Editer - categorie')

@section('contenu')
<div class="card">
    <div class="card-header">Editer une categorie</div>
    <div class="card-content">
        <form class="form-material" method="POST" action="{{route('categories.update', ['categorie' => $categorie->id])}}">
            @method('PUT')
            @csrf
            <div class="grix xs1">
                <div class="form-field">
                    <input type="text" id="title" name="name" class="form-control" value="{{$categorie->name}}" />
                    <label for="title">Titre</label>
                </div>
                <div class="form-field">
                    <label for="description">Description</label>
                    <textarea id="description" name="description" class="form-control">{{$categorie->description}}</textarea>
                </div>
            </div>
            <button type="submit" class="btn press secondary">Mettre à jour</button>
        </form>
    </div>
</div>
@endsection
