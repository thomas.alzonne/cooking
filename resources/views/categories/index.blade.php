@extends('template.app')

@section('title', 'Mes categories')

@section('contenu')
<div class="responsive-table">
    <table class="table">
        <thead>
            <tr>
                <th>#</th>
                <th>Nom</th>
                <th>Description</th>
            </tr>
        </thead>
        <tbody>
          <a href="{{route('categories.create')}}"class="btn blue"
        <span class="outline-text">Ajouter categorie</span>
      </a>
            @foreach($categories as $categorie)
            <tr>
                <td>{{$categorie->id}}</td>
                <td>{{$categorie->name}}</td>
                <td>{{$categorie->description}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
