@extends('template.app')

@section('title', 'Mes ingredients')

@section('contenu')
<div class="responsive-table">
    <table class="table">
        <thead>
            <tr>
                <th>#</th>
                <th>Nom</th>
                <th>Description</th>
            </tr>
        </thead>
        <tbody>
          <a href="{{route('ingredients.create')}}"class="btn blue"
        <span class="outline-text">Ajouter ingredient</span>
      </a>
            @foreach($ingredients as $ingredient)
            <tr>
                <td>{{$ingredient->id}}</td>
                <td>{{$ingredient->name}}</td>
                <td>{{$ingredient->description}}</td>
                <td>
                <div class="flex">
                    <a href="{{route('ingredients.edit', ['ingredient' => $ingredient->id])}}" class="btn circle secondary">
                        <i class="fas fa-pen"></i>
                    </a>
                    <form method="POST" action="{{route('ingredients.destroy', ['ingredient' => $ingredient->id])}}">
                        @method('DELETE')
                        @csrf
                        <button type="submit" class="ml-2 btn circle red dark-1">
                            <i class="fas fa-trash"></i>
                        </button>
                    </form>
                </div>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
