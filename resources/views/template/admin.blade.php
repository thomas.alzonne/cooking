<html>
  <head>
    <title>@yield('title')</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <!-- Import font awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/axentix@0.4.2/dist/css/axentix.min.css" />
  </head>

  <body class="layout">
    <header>
      <nav class="navbar red dark-1">
        <a href="#" target="_blank" class="navbar-brand">Thomas</a>
        <div class="navbar-menu ml-auto">
          <a class="navbar-link" href="#">Home</a>
        </div>
      </nav>
    </header>

    <main>
        @yield('contenu')
    </main>

    <footer class="footer red dark-1">
      Copyright © 2020 - Thomas Alzonne
    </footer>

    <script src="https://cdn.jsdelivr.net/npm/axentix@0.4.2/dist/js/axentix.min.js"></script>
  </body>
</html>
