@extends('template.admin')

@section('title', 'Editer - Recette')

@section('contenu')
<div class="card">
    <div class="card-header">Editer une recette</div>
    <div class="card-content">
        <form class="form-material" method="POST" action="{{route('recettes.update', ['recette' => $recette->id])}}">
            @method('PUT')
            @csrf
            <div class="grix xs1">
                <div class="form-field">
                    <input type="text" id="title" name="name" class="form-control" value="{{$recette->name}}" />
                    <label for="title">Titre</label>
                </div>
                <div class="form-field">
                    <label for="description">Description</label>
                    <textarea id="description" name="description" class="form-control">{{$recette->description}}</textarea>
                </div>
            </div>
            <button type="submit" class="btn press secondary">Mettre à jour</button>
        </form>
    </div>
</div>
@endsection
