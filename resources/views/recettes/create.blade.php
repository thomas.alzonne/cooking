@extends('template.app')

@section('title', 'Créer - Recette')

@section('contenu')
<div class="card">
    <div class="card-header">Créer une recette</div>
    <div class="card-content">
        <form class="form-material" method="POST" action="{{route('recettes.store')}}">
            @csrf
            <div class="grix xs1">
                <div class="form-field">
                    <input type="text" id="title" name="name" class="form-control" />
                    <label for="title">Nom</label>
                </div>
                <div class="form-field">
                    <label for="description">Description</label>
                    <textarea id="description" name="description" class="form-control"></textarea>
                </div>

                <div class="form-field form-default">
                  <label class="txt-center" for="select">Catégorie</label>
                      <select class="form-control" id="select" name="categorie">
                        <?php
                        foreach ($categories as $categorie)
                            {
                                echo "<option>";
                                echo $categorie;
                                echo "</option>";
                            }
                         ?>
                      </select>
                </div>


                <div class="form-field form-default">
                  <label class="txt-center" for="select1">Ingredient 1</label>
                      <select multiple class="form-control" id="select1" name="ingredient[]">

                        @foreach ($ingredients as $ingredient)
                        <option value="{{$ingredient->id}}"> {{ $ingredient->name }}</option>
                         @endforeach
                      </select>
                </div>
            </div>
            <button type="submit" class="btn press primary">Créer</button>
        </form>
    </div>
</div>
@endsection
