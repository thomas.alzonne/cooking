@extends('template.app')

@section('title', 'Mes recettes')

@section('contenu')
<div class="responsive-table">
    <table class="table">
        <thead>
            <tr>
                <th>#</th>
                <th>Nom</th>
                <th>Catégorie</th>
                <th>Description</th>
            </tr>
        </thead>
        <tbody>
            @foreach($recettes as $recette)
            <tr>
                <td>{{$recette->id}}</td>
                <td>{{$recette->name}}</td>
                <td>{{$recette->categorie}}</td>
                <td>{{$recette->description}}</td>
                <td>
                <div class="flex">
                    <a href="{{route('recettes.edit', ['recette' => $recette->id])}}" class="btn circle secondary">
                        <i class="fas fa-pen"></i>
                    </a>
                    <form method="POST" action="{{route('recettes.destroy', ['recette' => $recette->id])}}">
                        @method('DELETE')
                        @csrf
                        <button type="submit" class="ml-2 btn circle red dark-1">
                            <i class="fas fa-trash"></i>
                        </button>
                    </form>
                    <div class="modal white rounded-3" id="modal-example">
  <div class="modal-header">
    Recette : {{$recette->name}}
    <button data-target="modal-example" class="modal-trigger">
      <i class="fas fa-times"></i> <!-- Use font awesome -->
    </button>
  </div>
  <div class="modal-content">
    <ul>
      <li>{{$recette->ingredient1}}</li>
      <li>{{$recette->ingredient2}}</li>
      <li>{{$recette->ingredient3}}</li>
      <li>{{$recette->ingredient4}}</li>
      <li>{{$recette->ingredient5}}</li>
    </ul>
  </div>
  <div class="modal-footer">
    {{$recette->description}}
  </div>
</div>

<button class="btn primary press modal-trigger" data-target="modal-example">Détails</button>
                </div>
            </tr>
            @endforeach

        </tbody>

    </table>
    <br>
    <a href="{{route('recettes.create')}}"class="btn blue"
  <span class="outline-text">Ajouter recette</span>
</a>
</div>
@endsection
