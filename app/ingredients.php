<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\recettes;

class ingredients extends Model
{
  public function recettes()
 {
     return $this->belongsToMany(recettes::class, 'ingredients_recettes');
 }
}
