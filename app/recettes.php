<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class recettes extends Model
{
  public function ingredients()
 {
     return $this->belongsToMany(ingredients::class,'ingredients_recettes', 'recette_id','ingredient_id');
 }
}
