<?php

namespace App\Http\Controllers;

use App\categories;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $categories = categories::all();
      return view('categories.index',['categories' => $categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
     {
       $inputs = $request->except('_token');
       $categorie = new categories();
       foreach ($inputs as $key => $value) {
           $categorie->$key = $value;
       }
       $categorie->save();

       return redirect(route('categories.index'))->with('success', 'Experience enregistré avec succès !');
     }

    /**
     * Display the specified resource.
     *
     * @param  \App\categories  $categories
     * @return \Illuminate\Http\Response
     */
    public function show(categories $categories)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\categories  $categories
     * @return \Illuminate\Http\Response
     */
     public function edit($id)
     {
       $categorie = categories::find($id);
       return view('categories.edit', ['categorie' => $categorie]);
     }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\categories  $categories
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request, $id)
     {
       $inputs = $request->except('_token', '_method');
       $categorie = categories::find($id);
       foreach ($inputs as $key => $value) {
         $categorie->$key = $value;
       }
       $categorie->save();

       return redirect(route('categories.index'))->with('success', 'Experience mis a jour avec succès !');
     }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\categories  $categories
     * @return \Illuminate\Http\Response
     */
     public function destroy($id)
     {
         $categorie = categories::find($id);
         $categorie->delete();

         return redirect(route('categories.index'))->with('succes', 'Experience mis à jour avec succès !');
     }
}
