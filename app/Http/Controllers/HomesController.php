<?php

namespace App\Http\Controllers;

use App\homes;
use App\recettes;
use DB;
use Illuminate\Http\Request;

class HomesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $recettes = DB::table('recettes')->pluck('id','name','description')->all();
      return view('homes.index',['recettes' => $recettes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\homes  $homes
     * @return \Illuminate\Http\Response
     */
    public function show(homes $homes)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\homes  $homes
     * @return \Illuminate\Http\Response
     */
    public function edit(homes $homes)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\homes  $homes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, homes $homes)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\homes  $homes
     * @return \Illuminate\Http\Response
     */
    public function destroy(homes $homes)
    {
        //
    }
}
