<?php

namespace App\Http\Controllers;

use App\recettes;
use App\ingredients;
use DB;
use Illuminate\Http\Request;

class RecettesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $recettes = recettes::all();
      return view('recettes.index',['recettes' => $recettes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $ingredients = ingredients::all();
        $categories = DB::table('categories')->pluck('name');
        return view('recettes.create',['ingredients' => $ingredients],['categories' => $categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $inputs = $request->except('_token');
      $recette = new recettes();
      $recette->name = $request->input('name');
      $recette->description = $request->input('description');
      $recette->categorie = $request->input('categorie');
      $recette->save();

      $ingredients = ingredients::find($request->input('ingredient'));
      $recette->ingredients()->attach($ingredients);
      return redirect(route('recettes.index'))->with('success', 'Recette enregistré avec succès !');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\recettes  $recettes
     * @return \Illuminate\Http\Response
     */
    public function show(recettes $recettes)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\recettes  $recettes
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $recette = recettes::find($id);
      return view('recettes.edit', ['recette' => $recette]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\recettes  $recettes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $inputs = $request->except('_token', '_method');
      $recette = recettes::find($id);
      foreach ($inputs as $key => $value) {
        $recette->$key = $value;
      }
      $recette->save();

      return redirect(route('recettes.index'))->with('success', 'Experience mis a jour avec succès !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\recettes  $recettes
     * @return \Illuminate\Http\Response
     */
     public function destroy($id)
     {
         $recette = recettes::find($id);
         $recette->delete();

         return redirect(route('recettes.index'))->with('succes', 'Experience mis à jour avec succès !');
     }
}
