<?php

namespace App\Http\Controllers;

use App\ingredients;
use Illuminate\Http\Request;

class IngredientsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $ingredients = ingredients::all();
      return view('ingredients.index',['ingredients' => $ingredients]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('ingredients.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
     {
       $inputs = $request->except('_token');
       $ingredient = new ingredients();
       foreach ($inputs as $key => $value) {
           $ingredient->$key = $value;
       }
       $ingredient->save();

       return redirect(route('ingredients.index'))->with('success', 'Experience enregistré avec succès !');
     }

    /**
     * Display the specified resource.
     *
     * @param  \App\ingredients  $ingredients
     * @return \Illuminate\Http\Response
     */
    public function show(ingredients $ingredients)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ingredients  $ingredients
     * @return \Illuminate\Http\Response
     */
     public function edit($id)
     {
       $ingredient = ingredients::find($id);
       return view('ingredients.edit', ['ingredient' => $ingredient]);
     }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ingredients  $ingredients
     * @return \Illuminate\Http\Response
     */
     public function update(Request $request, $id)
     {
       $inputs = $request->except('_token', '_method');
       $ingredient = ingredients::find($id);
       foreach ($inputs as $key => $value) {
         $ingredient->$key = $value;
       }
       $ingredient->save();

       return redirect(route('ingredients.index'))->with('success', 'Experience mis a jour avec succès !');
     }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ingredients  $ingredients
     * @return \Illuminate\Http\Response
     */
     public function destroy($id)
     {
         $ingredient = ingredients::find($id);
         $ingredient->delete();

         return redirect(route('ingredients.index'))->with('succes', 'Experience mis à jour avec succès !');
     }
}
